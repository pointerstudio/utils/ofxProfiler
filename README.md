ofxProfile
================

![thumbnail](ofxaddons_thumbnail.png)

OpenFrameworks addon for simple and quick thread safe profiling.
Then throw the result in chrome://tracing

This addon is practically cut and paste from https://github.com/TheCherno/Hazel/blob/master/Hazel/src/Hazel/Debug/Instrumentor.h


## Usage

add ofxProfiler to your application with projectGenerator (or whatever method you prefer)

ofApp.h
```
// uncommment to profile
// #define OFX_PROFILER 1

#include "ofxProfiler.h"

// ... bla bla bla
```

ofApp.cpp
```
//--------------------------------------------------------------
void ofApp::setup() {
    OFX_PROFILER_BEGIN_SESSION("a test","result.json");
    OFX_PROFILER_FUNCTION();

    
} // end of scope => profile function ends

//--------------------------------------------------------------
void ofApp::update() {
    OFX_PROFILER_FUNCTION();

    {
        OFX_PROFILER_SCOPE("scope 1");
    }

    {
        OFX_PROFILER_SCOPE("scope 2");
    }
}

//--------------------------------------------------------------
void ofApp::exit() {
    OFX_PROFILER_END_SESSION();
}
```

when closing the application, you will find a result.json next to the binary
open that with chrome://tracing
